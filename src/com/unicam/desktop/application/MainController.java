package com.unicam.desktop.application;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.unicam.collaborations.bfs.CollaborationBDF;
import com.unicam.collaborations.bfs.Graph;
import com.unicam.collaborations.bfs.Node;
import com.unicam.collaborations.visualization.VisualGraph;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;




public class MainController implements Initializable {

	String choreographyPath, collaborationPath;

	@FXML
	private Button button_collaborations, buttun_verification, show_graph;

	@FXML
	private ListView listview_collaborations;

	@FXML
	private TextArea text_result;


	@FXML
	private ImageView image_safe, image_sound;





	CollaborationBDF collaboration;
	String modelFile;




	public void Button_load_collaborations(ActionEvent event) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open Files");
		//		fileChooser.setInitialDirectory(new File("CollaborationRepository/"));
		fileChooser.getExtensionFilters().addAll(new ExtensionFilter("Collaboration&Choreography", "*.bpmn"),
				new ExtensionFilter("All Files", "*.*"));
		List<File> selectedFile = fileChooser.showOpenMultipleDialog(null);
		if (selectedFile != null) {
			for (File file : selectedFile) {
				listview_collaborations.getItems().add(file.getAbsolutePath());
			}
			listview_collaborations.getSelectionModel().select(0);
		}

	}



	public void showGraph() {
		Graph<Node> LTS = collaboration.getLTS();
		String[] fileName=modelFile.split("/");
		VisualGraph graphStream=new VisualGraph(fileName[fileName.length-1]);
		graphStream.showGraph();
		for (Node n : LTS.map.keySet()) {
			graphStream.getGraph().addNode(String.valueOf(n.getId())).setAttribute("ui.label", n.toString());
		}
		int i=0;
		for (Node n : LTS.map.keySet()) {
			for (Node child : LTS.map.get(n).keySet()) {
				if (child!=null&&n!=null) {
					graphStream.getGraph().addEdge(String.valueOf(i++),String.valueOf(n.getId()), String.valueOf(child.getId())).setAttribute("ui.label", LTS.map.get(n).get(child));
				}

			}
		}

	}





	public void Button_verification(ActionEvent event) {
		int collaborations = listview_collaborations.getSelectionModel().getSelectedIndex();

		if (collaborations == -1 ) {
			Alert alert = new Alert(AlertType.INFORMATION,
					"Please select at least one collaboration and one \n choreography", ButtonType.OK);
			alert.showAndWait();
		} else {

			text_result.setText("Please attend. The verification Process is running!!");

			modelFile=listview_collaborations.getItems().get(collaborations).toString();		
			collaboration = new CollaborationBDF();
			JSONObject verificationResult = null;
			try {
				File f=File.createTempFile(modelFile, null);
				verificationResult=collaboration.init(new FileInputStream(modelFile), false);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();

			}
			JSONObject models =new JSONObject();
			JSONArray currentModel=new JSONArray();
			currentModel.put(verificationResult);

			try {
				models.put("models", currentModel);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				text_result.setText(models.toString(5));
				int sound= (int) models.getJSONArray("models").getJSONObject(0).getJSONObject("sound").get("id");
				int safe= (int) models.getJSONArray("models").getJSONObject(0).getJSONObject("safe").get("id");
				if (sound==0 || sound==1) {
					image_sound.setImage(new Image("com/unicam/desktop/application/images/red.png"));
				}if (sound==2) {
					image_sound.setImage(new Image("com/unicam/desktop/application/images/yellow.png"));
				}if (sound==3) {
					image_sound.setImage(new Image("com/unicam/desktop/application/images/green.png"));
				}
				if (safe==4) {
					image_safe.setImage(new Image("com/unicam/desktop/application/images/green.png"));
				}
				if (safe==5) {
					image_safe.setImage(new Image("com/unicam/desktop/application/images/red.png"));
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//		image_safe.setImage(new Image("com/unicam/desktop/application/images/yellow.png"));



			show_graph.setDisable(false);

		}

	}







	@Override
	public void initialize(URL location, ResourceBundle resources) {


	}

}