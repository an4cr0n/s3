package com.unicam.collaborations.bfs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Supplier;

import org.json.JSONException;
import org.json.JSONObject;

public class MassiveTestFromfolder {

	public static void main(String[] args) throws FileNotFoundException {
		final ExecutorService service = Executors.newFixedThreadPool(1);
		

		try {
			final FileWriter results = new FileWriter(new File("resultsSingleThread.txt"));

			Map<Integer, String> codes = new HashMap<Integer,String>(4);
			codes.put(0,"Unsound for dead token");
			codes.put(1,"Unsound for proper completion violation");
			codes.put(2,"Message disregarding sound");
			codes.put(3,"Sound"); 
			codes.put(4,"Safe");
			codes.put(5,"Unsafe"); 
			
			File folder = new File("/home/cippus/Downloads/Validazione Studenti-20191218T155502Z-001/Validazione Studenti/Scenario 2");
			File[] listOfFiles = folder.listFiles();
			for (int i = 0; i < listOfFiles.length; i++) {

				if (listOfFiles[i].getName().contains(".bpmn")) {
					File input = listOfFiles[i];
					//System.out.println(input);
					CollaborationBDF c=new CollaborationBDF();

					Executor executorService= Executors.newSingleThreadExecutor();
					FileInputStream fileInput=new FileInputStream(input);
					CompletableFuture.supplyAsync(() -> {
						return c.init(fileInput, false);

					}, executorService)
					.whenComplete((result, error) -> {
						if (error == null) {
							int soundCode;

						
								try {
									soundCode = (int) result.getJSONObject("sound").get("id");
									int safeCode = (int) result.getJSONObject("safe").get("id");
									results.write(input.getName() + "\t" + result.get("elements") + "\t" + result.get("time")
									+ "\t" + soundCode + "\t"
									+ result.getJSONObject("sound").get("sequences") + "\t"
									+ result.getJSONObject("sound").get("messages") + "\t"
									+ safeCode + "\t"
									+ result.getJSONObject("safe").get("sequences") + "\t"
									+ result.getJSONObject("safe").get("messages") + "\t"
									+ codes.get(soundCode)+ "\t"
									+ codes.get(safeCode)+ "\n"); 
									results.flush();
								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								} catch (IOException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							
							


						} else {
							System.out.println("Sorry, we could not return you a result for model "+input);
						}
					});

				}	
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}

}
