package com.unicam.collaborations.visualization;

import org.graphstream.graph.implementations.SingleGraph;
import org.graphstream.ui.swingViewer.ViewPanel;
import org.graphstream.ui.view.Camera;
import org.graphstream.ui.view.Viewer;

public class VisualGraph {
	SingleGraph graph;
	public VisualGraph(String name) {
		graph =new SingleGraph(name);
		System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
		graph.addAttribute("ui.stylesheet", "url('"+ getClass().getResource("style.css").toString()+"')");

		graph.setStrict(false);
		graph.setAutoCreate(true);

		graph.addAttribute("ui.quality");
		graph.addAttribute("ui.antialias");
		graph.addAttribute("ui.default.title", name);
		
	}
	public void showGraph(){
		Viewer viewer = graph.display();
		viewer.enableAutoLayout();
		viewer.setCloseFramePolicy(Viewer.CloseFramePolicy.CLOSE_VIEWER);

		ViewPanel view = viewer.getDefaultView();
		view.resizeFrame(2048, 1080);
		view.setAutoscrolls(true);
		Camera camera = view.getCamera();
		camera.setAutoFitView(true);
//		camera.setViewPercent(0.5);
	}
	public SingleGraph getGraph() {
		return graph;
	}

}
